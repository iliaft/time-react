import React, {Component} from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import moment from 'moment';

import store from './store';
import Current from './components/Current';
import Selected from './components/Selected';
import FromNow from './components/FromNow';

// Stateless React Component
const Heading = () => {
    return <h1>React time</h1>
};
//Action creator's
function selectDate(date) {
    return {
        type: 'SELECT_DATE',
        selectedDate: date
    }
}
function fromDate(date1) {
    return {
        type: 'FROM_DATE',
        fromDate: date1
    }
}

// Main component
class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: moment()
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(date) {
        this.setState({
            startDate: date
        });
        let dateSend = moment(date).format('LL');
        let fromSend = moment(date).fromNow();
        store.dispatch(selectDate(dateSend));
        store.dispatch(fromDate(fromSend));
    }

    render() {
        return (
            <div>
                <Heading />
                <div className="flex-container">
                    <DatePicker
                        inline
                        selected={this.state.startDate}
                        onChange={this.handleChange}
                    />
                    <div>
                        <Selected />
                        <FromNow />
                        <Current />
                    </div>
                    <div>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
