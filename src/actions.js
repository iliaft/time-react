import moment from 'moment';

/*Types of actions*/
export const SET_DATE = 'SET_DATE';
export const SELECT_DATE = 'SELECT_DATE';
export const FROM_NOW_DATE = 'FROM_NOW_DATE';

export function selectDate(text) {
    return { type: SELECT_DATE,
        dateCurrent: moment().format('LTS') }
}