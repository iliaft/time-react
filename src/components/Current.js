import React from 'react'
import moment from 'moment';

class Current extends React.Component {
    constructor(props) {
        super(props);
        this.state = {date: moment().format('LTS')};
    }

    componentDidMount() {
        this.interval = setInterval(function() {
            this.setState({date: moment().format('LTS')})
        }.bind(this), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        return (
            <div><p>Current time: {this.state.date}</p></div>
        );
    }
}
export default Current