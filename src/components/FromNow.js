import React, { Component } from 'react'
import { connect } from 'react-redux';

class FromNow extends Component {
    render() {
        return <div><p>From now: {this.props.dateFrom}</p></div>
    }
}
// Connecting store and state
const mapStateToProps = function(state) {
    return {
        dateFrom: state.fromDate
    };
};

export default connect(mapStateToProps)(FromNow)