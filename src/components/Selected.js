import React, { Component } from 'react'
import { connect } from 'react-redux';

class Selected extends Component {
    render() {
        return <div><p>Selected time: {this.props.date}</p></div>
    }
}
// Connecting store and state
const mapStateToProps = function(state) {
    return {
        date: state.selectedDate
    };
};

export default connect(mapStateToProps)(Selected)