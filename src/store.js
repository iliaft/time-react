import { createStore } from 'redux';

const initialState = {
    selectedDate: 'pick date',
    fromDate: 'chose date'
};

// Reducer
function reducer(state = initialState, action) {
    switch ( action.type) {
        case 'SELECT_DATE': return Object.assign({}, state, { selectedDate: action.selectedDate });
        case 'FROM_DATE': return Object.assign({}, state, { fromDate: action.fromDate });
        default: return state;
    }
}

const store = createStore( reducer, initialState);

export default store;